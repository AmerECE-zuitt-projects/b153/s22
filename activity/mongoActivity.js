let user = `[
    {
        "_id": <ObjectId1>,
        "userName": "string",
        "userAge": "number",
        "userEmail": "string"
    }
]`;

let address = `[
    {
        "_id": <ObjectId2>
        "userId": <ObjectId1>
        "country": "string",
        "city": "string",
        "region": "string",
        "zipCode" "number",
        "barangay": "string",
        "street": "string"
    }
]`;

let product = `[
    {
        "_id": <ObjectId3>,
        "productName": "string",
        "productPrice": "number",
        "isAvailable": "boolean",
        "qty": "number"
        "description": "string",
    },
]`;

let orderSummary = `[
    {
        "_id": <ObjectId4>,
        "userId": <ObjectId1>,
        "productsOrdered": [
            {
                "productId": <ObjectId3>,
                "qty": "number"
            }
        ],
        "totalPayment": "number"
        "orderDate": "date"
    }
]`;