/* Data Model Design
    - is the step where the structure of all your services/application's data is decided, much like a blueprint is needed before building a house can begin.

    - Database: 
    All the collections compiled for a service/application's data
    e.g. a filing cabinet

    - Collections: separate categories complied  for a service/application's data 
    e.g. filing cabinet drawers
        - Users collection
        - Product collection
        - Orders collection

    - Documents: Each specified, individual records of resources
    e.g. Folders inside drawers
        - Specific user
        - Specific product
        - Specific order
    
    - Sub-documents: Specific information about a record that is complied similarly to a document, but is inside of a document itself
    e.g. file inside a folder
        - User orders
        - User contact info
        - User comments

    - Fields: Each record's specific information + their data type
    e.g. File/folder content
        - User's name = string
        - User's age = number
        - user's email = string
        - User's password = string
*/

// When starting to model your data, the very first question you need to ask is "What kind of information will i need to save to make my app/service work?"

// e.g. a blog
// Users
    // ID (MongoDB automatically adds an ID to all documents/records)
    // Username
    // Password
    // Email
    // isAdmin - boolean

// Contents/Posts
    // ID
    // title
    // body
    // datePosted - date
    // author - string/MongoDB ID

// When data is referenced, any other data
    /*
        Users document:
        {
            _id: <ObjectId1>,
            user: "123xyz"
        }
        Address document:
        {
            _id: <ObjectId2>
            userId: <ObjectId1>
            street: "123"
        }
    */
// When to use embedded vs referenced:
    //  Embedded vs referenced data often ony has "suggestions" instead of rules. if you believe that your data is more readable/makes more sense in one way, go ahead and so. Generally embedded data is easier to use and understand, EXCEPT for one specific scenario: if you 

/* Example
    User document:
    {
        _id: <ObjectId1>,
        user: "123xyz",
        orders: [
            {
                order data
            }
        ]
    }

    Products document:
    {
        userId: <Object
    }
*/